from flask import Flask
from flask import jsonify
from flask import Response
from unidecode import unidecode
import mysql.connector
import datetime 
from datetime import date
import sys
import time
import smtplib
import json
from email.mime.text import MIMEText
import collections
from collections import namedtuple
import requests
from urllib3.exceptions import InsecureRequestWarning
import hashlib

totalLeads = 0
totalLeadsNews = 0
totalLeadsUpdates = 0
totalLeadsNaoModificados = 0
totalEnviados = 0
totalErroEnviados = 0
totalInsert = 0
totalUpdate = 0

def noneToNull(pValue):
    value = str(pValue)
    if( value == 'None' ):
        return ''
    return value



def sendMail(subject, txtMessage):
    smtp_ssl_host = 'your_smtp.com.br'
    smtp_ssl_port = 465
    username = 'username'
    password = 'password'
    from_addr = 'youremail@test.com'
    to_addrs = ['youremail_1@test.com','youremail_2@test.com','youremail_3@test.com']
    message = MIMEText(txtMessage,'html')
    message['subject'] = subject
    message['from'] = from_addr
    message['to'] = ', '.join(to_addrs)
    server = smtplib.SMTP_SSL(smtp_ssl_host, smtp_ssl_port)
    server.login(username, password)
    server.sendmail(from_addr, to_addrs, message.as_string())
    server.quit()



def formatDate(paramData):
    if( paramData != '' ):
        data = paramData[8:10] + '/' + paramData[5:7] + '/' + paramData[0:4]
    else:
        data = paramData
    return data



def refreshToken():
    root = collections.OrderedDict()
    root["client_id"] = "your client_id"
    root["client_secret"] ="your client_secret"
    root["refresh_token"] ="your refresh_token"

    requests.packages.urllib3.disable_warnings() 
    response = requests.post('https://api.rd.services/auth/token', headers={ 'Content-Type' : 'application/json' } ,data=json.dumps(root), verify=False)
    data = response.json()
    return data['access_token']



def getLeadByEmail(email, token):
    root = collections.OrderedDict()
    requests.packages.urllib3.disable_warnings() 
    response = requests.get('https://api.rd.services/platform/contacts/email:' + email, headers={ 'Content-Type' : 'application/json' , 'Authorization': 'Bearer ' + token }, verify=False)
    return response



def updateLead(lead, token, uuid):

    payload = collections.OrderedDict()
    payload["name"] = lead['NOME']
    payload["email"] = lead['EMAIL']
    payload["job_title"] = ""
    payload["state"] = lead['ESTADO']
    payload["city"] = lead['CIDADE']
    payload["country"] = "Brasil"
    payload["personal_phone"] = ""
    payload["mobile_phone"] = ""
    payload["website"] = ""

    #custom_fields
    payload["your_field_name"] = ''
    #custom_fields

    # caso seja necessario tags adicione aqui
    tags = []
    #tags.append('to_delete')
    #tags.append('2021')
    payload["tags"] = tags

    legalBases = []

    legalBase = collections.OrderedDict()
    legalBase["category"] = "data_processing"
    legalBase["type"] = "pre_existent_contract"
    legalBases.append(legalBase)

    legalBase = collections.OrderedDict()
    legalBase["category"] = "communications"
    legalBase["type"] = "consent"
    legalBase["status"] = "granted"
    legalBases.append(legalBase)

    payload["legal_bases"] = legalBases
    requests.packages.urllib3.disable_warnings() 
    response = requests.patch('https://api.rd.services/platform/contacts/' + uuid, headers={ 'Content-Type' : 'application/json' , 'Authorization': 'Bearer ' + token } ,data=json.dumps(payload), verify=False)
    return response



def createLead(lead, token):

    root = collections.OrderedDict()
    root["event_type"] = "CONVERSION"
    root["event_family"] ="CDP"

    payload = collections.OrderedDict()
    payload["conversion_identifier"] = "LEAD"
    payload["name"] = lead['NOME']
    payload["email"] = lead['EMAIL']
    payload["job_title"] = ""
    payload["state"] = lead['ESTADO']
    payload["city"] = lead['CIDADE']
    payload["country"] = "Brasil"
    payload["personal_phone"] = ""
    payload["mobile_phone"] = ""
    payload["twitter"] = ""
    payload["facebook"] = ""
    payload["linkedin"] = ""
    payload["website"] = ""
    payload["company_name"] = ""
    payload["company_site"] = ""
    payload["company_address"] = ""
    payload["client_tracking_id"] = ""
    payload["traffic_source"] = ""
    payload["traffic_medium"] = ""
    payload["traffic_value"] = ""

    #custom_fields
    payload["your_field_name"] = ''
    #custom_fields

    tags = []
    #tags.append('to_delete')
    #tags.append('2021')

    payload["tags"] = tags
    payload["available_for_mailing"] = True

    legalBases = []

    legalBase = collections.OrderedDict()
    legalBase["category"] = "data_processing"
    legalBase["type"] = "pre_existent_contract"
    legalBases.append(legalBase)

    legalBase = collections.OrderedDict()
    legalBase["category"] = "communications"
    legalBase["type"] = "consent"
    legalBase["status"] = "granted"
    legalBases.append(legalBase)

    payload["legal_bases"] = legalBases
    root["payload"] = payload
    requests.packages.urllib3.disable_warnings() 
    response = requests.post('https://api.rd.services/platform/events', headers={ 'Content-Type' : 'application/json' , 'Authorization': 'Bearer ' + token } ,data=json.dumps(root), verify=False)
    return response


# controla os hash dos dados pra não precisar
# ficar incluindo registros que não sofreram modificações
def insertHash(identificado_no_sistema_legado, email, rd_uuid, txthash):

    sqlInsert = "insert into migrate_leads ("
    sqlInsert+= " identificado_no_sistema_legado, "
    sqlInsert+= " email, "
    sqlInsert+= " rd_uuid, "
    sqlInsert+= " migration_at, "
    sqlInsert+= " txthash) values( "
    sqlInsert+= identificado_no_sistema_legado + ", "
    sqlInsert+= "'" + noneToNull(email) + "', "
    sqlInsert+= "'" + noneToNull(rd_uuid) + "', "
    sqlInsert+= "'" + noneToNull(datetime.datetime.now()) + "', "
    sqlInsert+= "'" + txthash + "')"
    mysqlCursor.execute(sqlInsert)


# atualiza os dados que sofreram modificações
def updateHash(id, rd_uuid, txthash, email):
       
    sqlUpdate = "update migrate_leads set "
    sqlUpdate+= " email = '" + email + "', "
    sqlUpdate+= " txthash = '" + txthash + "', "
    sqlUpdate+= " rd_uuid = '" + rd_uuid + "', "
    sqlUpdate+= " migration_at = '" + noneToNull(datetime.datetime.now()) + "' "
    sqlUpdate+= " where id = " + str( id )
    mysqlCursor.execute(sqlUpdate)
        


def getHashLead(identificado_no_sistema_legado):

    query = "select id, identificado_no_sistema_legado, email, txthash, rd_uuid, migration_at from migrate_leads where identificado_no_sistema_legado = '" + identificado_no_sistema_legado + "'"
    mysqlCursor.execute(query)
    rows = mysqlCursor.fetchall()    
    pessoas = ''
    objectList = []

    for row in rows:
        hashLead = collections.OrderedDict()
        hashLead['id'] = noneToNull( row[0] )
        hashLead['identificado_no_sistema_legado'] = noneToNull( row[1] )
        hashLead['email'] = noneToNull( row[2] )
        hashLead['txthash'] = noneToNull( row[3] )
        hashLead['rd_uuid'] = noneToNull( row[4] )
        hashLead['migration_at'] = str( row[5] )
        objectList.append( hashLead )
    return objectList



def getLeads():
    
    global totalLeads
    global totalLeadsNews
    global totalLeadsUpdates
    global totalLeadsNaoModificados
    global totalErroEnviados

    #sua query para pegar a lista de leads
    query = "select * from seus leads"

    mysqlCursor.execute(query)
    rows = mysqlCursor.fetchall()    

    totalLeads = len( rows )
    #print('Lendo ' + str( totalLeads ) + ' registros' )

    pessoas = ''
    contador = 0
    objectList = []
    for row in rows:

        lead = collections.OrderedDict()
        lead['NOME'] = noneToNull( row[0] )
        lead['EMAIL'] = noneToNull( row[1] )

        strHash = str(json.dumps( lead ))
        result = hashlib.md5( strHash.encode() )
        savedLead = getHashLead( noneToNull( row[3] ) )
        lead['LAST_UPDATE'] = formatDate( str(datetime.datetime.now()) )

        if len( savedLead ) == 0 :

            lead['HASH'] = result.hexdigest()
            lead['ID'] = 0
            lead['ACTION'] = 'INSERT'
            totalLeadsNews+=1
            objectList.append( lead )
        else:

            if savedLead[0]['txthash']  != result.hexdigest() :
                lead['HASH'] = result.hexdigest()
                lead['ID'] = savedLead[0]['id']
                lead['ACTION'] = 'UPDATE'
                if( savedLead[0]['email'] != lead['EMAIL'] ):
                    lead['LAST_EMAIL'] = savedLead[0]['email']
                else:
                    lead['LAST_EMAIL'] = ''

                totalLeadsUpdates+=1
                objectList.append( lead )

            else:
                totalLeadsNaoModificados+=1
        contador+=1
        #print( "contador: " + str(contador) )
    return  objectList


try:

    mysqlconn = mysql.connector.connect(
    host="localhost",
    user="root",
    password="password",
    database="database",
    ssl_disabled=True
    )

    mysqlconn.autocommit = False
    mysqlCursor = mysqlconn.cursor()
    inicio = str(datetime.datetime.now())
    print("Inicio: " + inicio)
    token = refreshToken()
    leads = getLeads()

    contadorLeads = 0
    contadorTotalLeads = 0
    horaInicio = ''
    horaFim = ''
    if( len(leads) > 0 ):

        for lead in leads:
            if( contadorLeads == 120 ):
                horaFim = datetime.datetime.now()
                diferenca = (horaFim - horaInicio)
                segundos = diferenca.seconds
                if( segundos <= 60 ):
                    segundos = ( 60 - segundos )
                    time.sleep(segundos)
                else:
                    segundos =  60 - (segundos % 60)
                    time.sleep(segundos)
                contadorLeads = 0

            if( contadorLeads == 0 ):
                horaInicio = datetime.datetime.now()

            if( lead['ACTION'] == 'INSERT' ):

                ret = createLead( lead, token )

                if( str(ret) == '<Response [200]>' ):
                    retorno = ret.json()

                    try:
                        insertHash( lead['identificado_no_sistema_legado'], lead['EMAIL'], retorno['event_uuid'], lead['HASH'] )
                        mysqlconn.commit()
                        totalInsert+=1

                    except Exception as error:
                        mysqlconn.rollback()

                else:
                    totalErroEnviados+=1   

            else:

                if( lead['LAST_EMAIL'] != '' ):
                    oldData = getLeadByEmail(lead['LAST_EMAIL'], token)
                    data = oldData.json()

                    retUpdate = updateLead(lead, token, data['uuid'])

                    if( str(retUpdate) == '<Response [200]>' ):
                        ret = createLead( lead, token )

                        if( str(ret) == '<Response [200]>' ):
                            retorno = ret.json()
                            #print( retorno['event_uuid'] )

                            try:
                                updateHash( lead['ID'], retorno['event_uuid'], lead['HASH'], lead['EMAIL'] )
                                mysqlconn.commit()
                                totalUpdate+=1

                            except Exception as error:
                                mysqlconn.rollback()

                        else:
                            totalErroEnviados+=1   

                else:

                    ret = createLead( lead, token )                    
                    if( str(ret) == '<Response [200]>' ):
                        retorno = ret.json()
                        #print( retorno['event_uuid'] )

                        try:
                            updateHash( lead['ID'], retorno['event_uuid'], lead['HASH'], lead['EMAIL'] )
                            mysqlconn.commit()
                            totalUpdate+=1

                        except Exception as error:
                            mysqlconn.rollback()

                    else:
                        totalErroEnviados+=1   

            contadorLeads+=1
            contadorTotalLeads+=1
            #print("contadorTotalLeads: " + str(contadorTotalLeads))

    fim = str(datetime.datetime.now())
    #mysqlconn.commit()
    print("Fim: " + fim)


    htmlList = ''
    htmlList = '<table border="1" style="border:1px solid #000;"><thead><tr style="background-color:#cccccc;"><th colspan="2">RELATÓRIO DE IMPORTAÇÃO</th></tr></thead><tbody>'
    htmlList+='<tr>'
    htmlList+='<td style="width:250px;">TOTAL CONTATOS:</td><td style="width:250px;">'+ str(totalLeads) +'</td>'
    htmlList+='</tr>'
    htmlList+='<tr>'
    htmlList+='<td>NOVOS</td><td>'+ str(totalLeadsNews) +'</td>'
    htmlList+='</tr>'
    htmlList+='<tr>'
    htmlList+='<td>NOVOS SALVOS</td><td>'+ str(totalInsert) +'</td>'
    htmlList+='</tr>'
    htmlList+='<tr>'
    htmlList+='<td>ATUALIZADOS</td><td>'+ str(totalLeadsUpdates) +'</td>'
    htmlList+='</tr>'
    htmlList+='<tr>'
    htmlList+='<td>ATUALIZADOS SALVOS</td><td>'+ str(totalUpdate) +'</td>'
    htmlList+='</tr>'

    htmlList+='<tr>'
    htmlList+='<td>NÃO MODIFICADOS</td><td>'+ str(totalLeadsNaoModificados) +'</td>'
    htmlList+='</tr>'
    htmlList+='<tr>'
    htmlList+='<td>INICIO</td><td>'+ inicio +'</td>'
    htmlList+='</tr>'
    htmlList+='<tr>'
    htmlList+='<td>FIM</td><td>'+ fim +'</td>'
    htmlList+='</tr>'
    htmlList+='</tbody></table>'

        
    sendMail('Migração RD Station', htmlList)
   
except Exception as error:
    sendMail('Erro migração RD Station', 'O seguinte erro ocorreu: ' + str(error))

finally:
    if ( mysqlconn.is_connected() ):
        mysqlCursor.close()
        mysqlconn.close()
        print( 'Conexao mysqlconn fechada' )
